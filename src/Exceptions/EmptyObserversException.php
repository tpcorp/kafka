<?php

namespace Coursondev\Kafka\Exceptions;

use Exception;

class EmptyObserversException extends Exception
{
}
