<?php

namespace Coursondev\Kafka\Exceptions;

use InvalidArgumentException;

class NotReadableException extends InvalidArgumentException
{
}
