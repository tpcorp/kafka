<?php

namespace Coursondev\Kafka\Exceptions;

use InvalidArgumentException;

class NotSendException extends InvalidArgumentException
{
}
