<?php

declare(strict_types=1);

namespace Coursondev\Kafka;

class Topics
{
    const VOPROS_COMPANIES       = 'vopros.companies';
    const VOPROS_COMPANY_PERSONS = 'vopros.company.persons';

    const VOPROS_ORDERS            = 'vopros.orders';
    const VOPROS_EDUCATION_GROUPS  = 'vopros.education.groups';
    const VOPROS_EDUCATION_PERSONS = 'vopros.education.persons';

    const VOPROS_SERVICE_APPENDED = 'vopros.service.appended';

    const COURSON_ORDERS    = 'courson.orders';
    const COURSON_STUDENTS  = 'courson.students';
    const COURSON_ORDER_LOG = 'courson.order.log';

    const CORP_NOTIFICATIONS_MESSAGE = 'corp.notifications.message';

    const COURSON_METRIC_EVENTS = 'courson.metric.events';
}