<?php

declare(strict_types=1);

namespace Coursondev\Kafka\Messages;

interface FillableInterface
{
    /**
     * @return array
     */
    public function getFillable(): array;
}