<?php

namespace Coursondev\Kafka\Messages;

class ExampleMessage extends AbstractMessage
{
    /** @var array */
    protected $fillable = [
        'uuid',
        'name',
    ];
}