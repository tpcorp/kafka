<?php

namespace Coursondev\Kafka\Messages\Metrics;

use Coursondev\Kafka\Messages\AbstractMessage;
use Coursondev\Kafka\Messages\FillableInterface;

class CreateMessage extends AbstractMessage implements FillableInterface
{
    /** @var array */
    protected $fillable = [
        'code',
        'data',
    ];

    /**
     * @inheritDoc
     */
    public function getFillable(): array
    {
        return $this->fillable;
    }
}