<?php

namespace Coursondev\Kafka\Messages;

class Event extends AbstractMessage
{
    /**
     * @param string $name
     * @param array $body
     */
    public function __construct(string $name, array $body)
    {
        $this->setBody($body);
        $this->setHeader(['name' => $name]);
    }

    /**
     * @param array $parameters
     * @return static
     */
    public static function byArray(array $parameters): self
    {
        $instance = new static($parameters['headers']['name'], $parameters['body']);

        $instance->setHeader($parameters['headers']);

        return $instance;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->headers['name'];
    }

    /**
     * @param array $attributes
     * @return self
     */
    public function setBody(array $attributes): self
    {
        $this->body = $attributes;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'body'    => $this->body,
            'headers' => array_merge($this->headers, [
                'name'          => $this->getName(),
                'builder_class' => get_class($this),
            ]),
        ];
    }
}