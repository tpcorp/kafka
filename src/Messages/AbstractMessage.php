<?php

namespace Coursondev\Kafka\Messages;

use stdClass;

abstract class AbstractMessage
{
    /** @var array */
    protected $headers = [];

    /** @var array */
    protected $body = [];

    /** @var array */
    protected $fillable = [];

    /**
     * @param string $system
     * @return static
     */
    public static function new(string $system): self
    {
        $instance = new static();

        $instance->headers['system'] = $system;

        return $instance;
    }

    /**
     * @param array $parameters
     * @return static
     */
    public static function byArray(array $parameters): self
    {
        $instance = new static();

        $instance->setBody($parameters['body'] ?? []);
        $instance->setHeader($parameters['headers'] ?? []);

        return $instance;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        if (!empty($this->headers['name'])) {
            return $this->headers['name'];
        }

        return $this->headers['builder_class'] ?? '';
    }

    /**
     * @return string
     */
    public function getSystem(): string
    {
        return $this->headers['system'];
    }

    /**
     * @param array $attributes
     * @return static
     */
    public function headers(array $attributes): self
    {
        $this->headers = array_merge($this->headers, $attributes);

        return $this;
    }

    /**
     * @param array $attributes
     * @return static
     */
    public function setHeader(array $attributes): self
    {
        $this->headers = $attributes;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers ?? [];
    }

    /**
     * @param array $attributes
     * @return self
     */
    public function setBody(array $attributes): self
    {
        $this->body = $this->recursiveFillable($attributes, $this->fillable);

        return $this;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body ?? [];
    }

    /**
     * Рекурсивно заполнить массив.
     *
     * @param array $attributes
     * @param array $fillable
     * @return array
     */
    private function recursiveFillable(array $attributes, array $fillable): array
    {
        $data = [];

        foreach ($fillable as $key => $value) {
            if (is_array($value)) {
                if (array_key_exists($key, $attributes)) {
                    $data[$key] = $this->recursiveFillable($attributes[$key], $value);
                }
            } else {
                if (array_key_exists($value, $attributes)) {
                    $data[$value] = $attributes[$value];
                }
            }
        }

        return $data;
    }

    /**
     * @return string
     */
    public function payload(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'body'    => $this->body,
            'headers' => array_merge($this->headers, [
                'builder_class' => get_class($this),
                'fillable'      => $this->fillable,
            ]),
        ];
    }
}
