<?php

namespace Coursondev\Kafka\Messages\Clients\Group;

use Coursondev\Kafka\Messages\AbstractMessage;

class CreateOrUpdateMessage extends AbstractMessage
{
    /** @var array */
    protected $fillable = [
        'uuid',
        'name',
        'number',
        'format',
        'max_students',
        'started_at',
        'exam_at',
        'link_schedule',
        'link_addition_materials',
        'link_form',
    ];
}