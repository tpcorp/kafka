<?php

namespace Coursondev\Kafka\Messages\Clients\Order;

use Coursondev\Kafka\Messages\AbstractMessage;

class CreateMessage extends AbstractMessage
{
    /** @var array */
    protected $fillable = [
        'order' => [
            'uuid',
            'number',
            'access_code',
            'is_paid',

            'responsible_contact_person' => [
                'uuid',
            ],

            'notifications' => [
                'auto_contact_person',
            ],

            'manager' => [
                'name',
            ],
        ],

        'company' => [
            'uuid',
            'name',
            'inn',
        ],

        'service' => [
            'uuid',
            'name',
        ],
    ];
}