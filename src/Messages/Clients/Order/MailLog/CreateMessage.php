<?php

declare(strict_types=1);

namespace Coursondev\Kafka\Messages\Clients\Order\MailLog;

use Coursondev\Kafka\Messages\AbstractMessage;

class CreateMessage extends AbstractMessage
{
    /** @var array */
    protected $fillable = [
        'order' => [
            'uuid',
        ],
        'message' => [
            'type',
            'to',
            'text',
            'timestamp',
        ],
        'initial_application_attributes',
    ];
}