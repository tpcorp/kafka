<?php

namespace Coursondev\Kafka\Messages\Clients\Student;

use Coursondev\Kafka\Messages\AbstractMessage;

class CreateMessage extends AbstractMessage
{
    /** @var array */
    protected $fillable = [
        'user' => [
            'uuid',
            'birth_date',
            'name',
            'middle_name',
            'last_name',
            'email',
            'phone',
            'snils',
            'work_experience',
            'location_address',
            'level_education',
            'nationality_id',
            'position',
            'status',
            'division_name',
            'position_type',
            'employment_date',
        ],
        'company' => [
            'uuid',
            'name',
            'inn',
        ],
        'student' => [
            'uuid',
        ],
    ];
}