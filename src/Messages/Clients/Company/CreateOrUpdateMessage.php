<?php

namespace Coursondev\Kafka\Messages\Clients\Company;

use Coursondev\Kafka\Messages\AbstractMessage;

class CreateOrUpdateMessage extends AbstractMessage
{
    /** @var array */
    protected $fillable = [
        'uuid',
        'name',
        'inn',
    ];
}