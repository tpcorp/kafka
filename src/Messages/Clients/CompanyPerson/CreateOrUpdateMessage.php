<?php

namespace Coursondev\Kafka\Messages\Clients\CompanyPerson;

use Coursondev\Kafka\Messages\AbstractMessage;

class CreateOrUpdateMessage extends AbstractMessage
{
    /** @var array */
    protected $fillable = [
        'user' => [
            'email',
            'name',
            'middle_name',
            'last_name',
            'birth_date',
            'phone',
            'position',
        ],

        'company' => [
            'uuid',
            'name',
            'inn',
        ],

        'person' => [
            'uuid',
        ],

        'responsible_for_orders',
    ];
}