<?php

namespace Coursondev\Kafka\Messages\Clients\Program;

use Coursondev\Kafka\Messages\AbstractMessage;

class SyncRelationsMessage extends AbstractMessage
{
    /** @var array */
    protected $fillable = [
        'order' => [
            'uuid',
        ],
        'program' => [
            'uuid',
        ],
        'group' => [
            'uuid',
        ],
    ];
}