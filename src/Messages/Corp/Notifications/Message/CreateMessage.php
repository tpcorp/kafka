<?php

namespace Coursondev\Kafka\Messages\Corp\Notifications\Message;

use Coursondev\Kafka\Messages\AbstractMessage;

class CreateMessage extends AbstractMessage
{
    /** @var array */
    public $fillable = [
        'action',
        'attributes',
    ];
}