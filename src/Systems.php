<?php

namespace Coursondev\Kafka;

class Systems
{
    const VOPROS  = 'vopros';
    const COURSON = 'courson';
    const METRICS = 'metrics';
}